import java.util.*;
import java.util.regex.*;

import edu.duke.fintech512.terms.*;
import edu.duke.ls435.assignment3.*;
import javafx.application.*;
import javafx.collections.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.layout.*;
import javafx.stage.*;

/**
 * The kwic program with gui
 *
 * @author Minglun Zhang
 */
public class KWICGUI extends Application {

    private ObservableList<String> titles;
    private ObservableList<String> ignoreWords;
    private ObservableList<String> printableList;
    private Pattern                my_pattern;

    /**
     * @param args
     */
    public static void main ( String[] args ) {
        launch( args );
    }

    @Override
    public void init () {
        my_pattern = Pattern.compile( "[^a-zA-Z ]" );
        titles = FXCollections.observableArrayList();
        ignoreWords = FXCollections.observableArrayList();
        printableList = FXCollections.observableArrayList();
    }

    @Override
    public void start ( Stage stage ) throws Exception {
        // set scene
        stage.setTitle( "KWIC GUI" );
        GridPane gridPane = new GridPane();
        Scene scene = new Scene( gridPane, 610, 330 );
        // scene.getStylesheets().add( "myStyle.css" );
        stage.setScene( scene );

        // set elements of ignore words
        Label outputLabel = new Label( "KWIC Output" );
        Button generate = new Button( "Generate" );
        ListView<String> printList = new ListView<String>();
        printList.setItems( printableList );
        generate.setOnAction( ( ActionEvent e ) -> generateKWIC() );

        // set elements of ignore words
        Label ignoreLabel = new Label( "Ignore Words" );
        Button addIgnore = new Button( "+" );
        addIgnore.setPrefHeight( 25 );
        addIgnore.setPrefWidth( 25 );
        Button deleteIgnore = new Button( "-" );
        deleteIgnore.setPrefHeight( 25 );
        deleteIgnore.setPrefWidth( 25 );
        TextField ignoreText = new TextField();
        ignoreText.setPromptText( "Enter a new ignore words" );
        ListView<String> ignoreList = new ListView<String>();
        ignoreList.setItems( ignoreWords );
        ignoreList.setOnMouseClicked( event -> {
            if ( ignoreList.getSelectionModel().getSelectedItem() != null ) {
                String selectedItem = ignoreList.getSelectionModel().getSelectedItem().toString();
                ignoreText.setText( selectedItem );
            }
        } );
        addIgnore.setOnAction( ( ActionEvent e ) -> addIgnore( ignoreText ) );
        deleteIgnore.setOnAction( ( ActionEvent e ) -> deleteIgnore( ignoreText ) );

        // set elements of titles
        Label titleLabel = new Label( "Titles" );
        Button addTitle = new Button( "+" );
        addTitle.setPrefHeight( 25 );
        addTitle.setPrefWidth( 25 );
        Button deleteTitle = new Button( "-" );
        deleteTitle.setPrefHeight( 25 );
        deleteTitle.setPrefWidth( 25 );
        TextField titleText = new TextField();
        titleText.setPromptText( "Enter a new title" );
        ListView<String> titleList = new ListView<String>();
        titleList.setItems( titles );
        titleList.setOnMouseClicked( event -> {
            if ( titleList.getSelectionModel().getSelectedItem() != null ) {
                String selectedItem = titleList.getSelectionModel().getSelectedItem().toString();
                titleText.setText( selectedItem );
            }
        } );
        addTitle.setOnAction( ( ActionEvent e ) -> addTitle( titleText ) );
        deleteTitle.setOnAction( ( ActionEvent e ) -> deleteTitle( titleText ) );

        // format the scene
        gridPane.setHgap( 10 );
        gridPane.setVgap( 10 );
        GridPane.setHalignment( titleLabel, HPos.CENTER );
        gridPane.add( titleLabel, 1, 1 );
        GridPane.setHalignment( ignoreLabel, HPos.CENTER );
        gridPane.add( ignoreLabel, 3, 1 );
        titleList.setPrefWidth( 300 );
        titleList.setPrefHeight( 100 );
        ignoreList.setPrefWidth( 200 );
        ignoreList.setPrefHeight( 100 );
        gridPane.add( titleList, 1, 2 );
        gridPane.add( ignoreList, 3, 2 );
        gridPane.add( titleText, 1, 3 );
        gridPane.add( ignoreText, 3, 3 );
        printList.setPrefWidth( 200 );
        printList.setPrefHeight( 100 );
        GridPane.setHalignment( outputLabel, HPos.CENTER );
        gridPane.add( outputLabel, 1, 4 );
        gridPane.add( printList, 1, 5 );

        VBox vbox = new VBox( 5 );
        vbox.getChildren().addAll( addTitle, deleteTitle );
        gridPane.add( vbox, 2, 2 );
        GridPane.setConstraints( vbox, 2, 2, 1, 2, HPos.CENTER, VPos.CENTER );
        VBox vbox2 = new VBox( 5 );
        vbox2.getChildren().addAll( addIgnore, deleteIgnore );
        gridPane.add( vbox2, 2, 2 );
        GridPane.setConstraints( vbox2, 4, 2, 1, 2, HPos.CENTER, VPos.CENTER );
        GridPane.setHalignment( generate, HPos.CENTER );
        GridPane.setValignment( generate, VPos.CENTER );
        gridPane.add( generate, 3, 5 );

        stage.show();
    }

    private void generateKWIC () {
        printableList.clear();
        List<Sentence> hisTitles = new ArrayList<>();
        titles.forEach( input -> {
            Sentence title = new Sentence( input );
            title.generateKeyWords( ignoreWords );
            hisTitles.add( title );
        } );
        Processor processor = new Index( hisTitles );
        processor.run();
        processor.sort();
        List<String> s = processor.print();
        s.forEach( line -> {
            printableList.add( line );
        } );

    }

    private void deleteIgnore ( TextField ignoreText ) {
        ignoreWords.remove( ignoreText.getText() );
        ignoreText.clear();
    }

    private void addIgnore ( TextField ignoreText ) {
        boolean check = my_pattern.matcher( ignoreText.getText() ).find();
        if ( check ) {
            Alert alert = new Alert( AlertType.WARNING );
            alert.setTitle( "Invalid Input" );
            alert.setHeaderText( null );
            alert.setContentText( "No charactoers other than a-z, A-Z and white space!" );
            alert.showAndWait();
        }
        else {
            ignoreWords.add( ignoreText.getText().toLowerCase() );
        }
        ignoreText.clear();
    }

    private void deleteTitle ( TextField titleText ) {
        titles.remove( titleText.getText() );
        titleText.clear();
    }

    private void addTitle ( TextField titleText ) {
        boolean check = my_pattern.matcher( titleText.getText() ).find();
        if ( check ) {
            Alert alert = new Alert( AlertType.WARNING );
            alert.setTitle( "Invalid Input" );
            alert.setHeaderText( null );
            alert.setContentText( "No charactoers other than a-z, A-Z and white space!" );
            alert.showAndWait();
        }
        else {
            titles.add( titleText.getText().toLowerCase() );
        }
        titleText.clear();

    }
}
