# FINTECH 512 Assignment 6

NOTE: For each task, take note of how long it took you, in hours and minues, and write it down.

**Read [1 hour, .5 points]**

NOTE: Credit for reading is established by writing and submitting notes on what you read.

On the Criteria To Be Used in Decomposing Systemsinto Modules and this summarization

**Watch [1 hour, .5 points]**

Is software engineering research addressing software engineering problems? Slides

**Coding [8+ hours, 4 points (1.5 for code, 2.5 for process)]**

Following the PSP, TDD, and refactoring practices discussed and practiced so far in class, write a JavaFX graphical user interface (GUI) for the KWIC app, using another student's terms package.

Find a collaborator, could be someone you've previously worked with, or someone new. Each of you will write your own GUI app.
Create a GitLab repo named 'assignment6' for this week's work. Add the 'FINTECH 512 Course Staff' group as a 'Reporter.'
Work through these JavaFX tutorials: Hello World, Form Design, Deployment Quickstart Consider typing the code in instead of copy/paste... you may find that doing so helps you understand and remember. NOTE: These tutorials are written for NetBeans, they will have to be adapted to the dev environment and/or Intellij. If you find easier tutorials to follow/adapt, please share them on Slack.

**GUI KWIC App requirements [1.5 points]**

Must produce the same output as assignment 4 for a given set of 'ignore words' and titles.
Must provide an editable field for displaying and entering 'ignore words'
Must provide an editable field for displaying and entering titles.
Must provide a non-editable field for displaying the output.
Must provide a button for generating output from the current set of 'ignore words' and titles.
Must use JavaFX.
Name the generated jar file 'KWICGUI.jar'
Beyond these requirements, you are free to keep things simple or add creativity as you see fit.

**Process requirements [2.5 points]**

You must use your collaborator's terms package from assignment5.
You must not make a copy of their source to change it. Changes to your collaborator's terms package may be accomplished by creating an issue in the collaborator's assignment5 repo, or by forking their repo, making the change, and submitting a merge request that the collaborator accepts.
All usual PSP issues must be created. Code review each other's GUI code and terms code changes per the standards in assignment 4.
A test suite is not required for the GUI controls, but if you figure out how to test them and create (and document how to run) a test suite for them, I will grant 1 extra credit point.
You must create a GitLab pipeline to pull the collaborator's latest terms package, run tests against it, builds your own app's source, and produce the final application.
You must add a Code Climate engine to the pipeline. Submit a report generated from the Code Climate engine. You are not required to fix any issues it reports, but you are welcome to do so if you see fit.
NOTE: Feel free to approach these tasks in the order that makes sense to you; follow the usual practices of PSP and frequent git commits.

**Reflection:**

Reflect on your work as soon you are done. What went well? What didn’t? What activities found the most bugs? What types of bugs caused you the most problems? Record your thoughts, but don’t try to solve problems. What did you do? What evidence did you leave? Is all of the above recorded in one or more GitLab issues?

**To Turn In**

Send me an email with your reflections and time estimates/actuals (copy/pasted from your Reflections issue) when you've completed your work.